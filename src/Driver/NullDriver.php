<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 7.4
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2020  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     3.1.0
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Driver;


/**
 * Cache backend for use without a cache server. Only does transient
 * in-memory caching
 */
class NullDriver extends AbstractDriver {

	/**
	 * In memory store
	 *
	 * @var array
	 */
	protected array $store = [];

	/**
	 * NullDriver constructor.
	 *
	 * @param array $config
	 * @param array $options
	 */
	public function __construct(array $config = [], array $options = [])
	{
		$this->store = [];
	}

	/**
	 * See if a key currently exists in the cache
	 *
	 * @param string $key
	 * @return bool
	 */
	public function exists(string $key): bool
	{
		return array_key_exists($key, $this->store);
	}

	/**
	 * Get the value for the selected cache key
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function get(string $key)
	{
		return $this->exists($key)
			? $this->store[$key]
			: NULL;
	}

	/**
	 * Set a cached value
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param int $expires
	 * @return bool
	 */
	public function set(string $key, $value, ?int $expires = NULL): bool
	{
		$this->store[$key] = $value;
		return $this->store[$key] === $value;
	}

	/**
	 * Remove an item from the cache
	 *
	 * @param string $key
	 * @return boolean
	 */
	public function delete(string $key): bool
	{
		// Don't return true if the key didn't exist to begin with
		if ( ! array_key_exists($key, $this->store))
		{
			return FALSE;
		}

		unset($this->store[$key]);
		return ( ! array_key_exists($key, $this->store));
	}

	/**
	 * Remove multiple items from the cache
	 *
	 * @param string[] $keys
	 * @return boolean
	 */
	public function deleteMultiple(array $keys = []): bool
	{
		$res = TRUE;

		foreach($keys as $key)
		{
			$res = $res && $this->delete($key);
		}

		return $res;
	}

	/**
	 * Empty the cache
	 *
	 * @return boolean
	 */
	public function flush(): bool
	{
		$this->store = [];
		return TRUE;
	}

	/**
	 * Set the specified key to expire at the given time
	 *
	 * @param string $key
	 * @param int $expires
	 * @return boolean
	 */
	public function expiresAt(string $key, int $expires): bool
	{
		//noop
		return array_key_exists($key, $this->store);
	}
}