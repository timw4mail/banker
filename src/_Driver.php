<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 7.4
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2020  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     3.1.0
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker;

use Aviat\Banker\Driver\DriverInterface;
use Aviat\Banker\Exception\InvalidArgumentException;

/**
 * Private trait for shared driver-related functionality
 */
trait _Driver {
	/**
	 * Driver class for handling the chosen caching backend
	 *
	 * @var DriverInterface
	 */
	private DriverInterface $driver;

	/**
	 * Instantiate the appropriate cache backend based on the config
	 *
	 * @param array $driverConfig
	 * @return DriverInterface
	 */
	protected function loadDriver(array $driverConfig = []): DriverInterface
	{
		$driver = ucfirst(strtolower($driverConfig['driver'] ?? 'null'));
		$class = __NAMESPACE__ . "\\Driver\\${driver}Driver";

		$driverConfig['connection'] = $driverConfig['connection'] ?? [];
		$driverConfig['options'] = $driverConfig['options'] ?? [];

		return new $class($driverConfig['connection'], $driverConfig['options']);
	}

	/**
	 * @param $keys
	 * @param bool $hash
	 * @throws InvalidArgumentException
	 */
	private function validateKeys($keys, bool $hash = FALSE): void
	{
		// Check type of keys
		if ( ! is_iterable($keys))
		{
			throw new InvalidArgumentException('Keys must be an array or a traversable object');
		}

		$keys = ($hash) ? array_keys((array)$keys) : (array)$keys;

		// Check each key
		array_walk($keys, fn($key) => $this->validateKey($key));
	}

	/**
	 * @param string $key
	 * @throws InvalidArgumentException
	 */
	private function validateKey($key): void
	{
		if ( ! is_string($key))
		{
			throw new InvalidArgumentException('Cache key must be a string.');
		}

		if (is_string($key) && preg_match("`[{}()/@:\\\]`", $key) === 1)
		{
			throw new InvalidArgumentException('Invalid characters in cache key');
		}
	}
}