#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && [[ ! -e /.dockerinit ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
#echo -e 'http://dl-cdn.alpinelinux.org/alpine/edge/main\nhttp://dl-cdn.alpinelinux.org/alpine/edge/community\nhttp://dl-cdn.alpinelinux.org/alpine/edge/testing' > /etc/apk/repositories
#apk upgrade --update --no-cache \
#	curl \
#	git

apt-get update && apt-get install -y \
	libz-dev \
	libmemcached-dev \
	git \
	curl

docker-php-ext-install zip
pecl install apcu
pecl install memcached
docker-php-ext-enable apcu memcached